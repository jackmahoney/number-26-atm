var AmountValidator = require('../../../src/scripts/models/AmountValidator');
var expect = require('chai').expect;

describe("AmountValidator", function(){

    it("0 should return too low", function(){
        expect(AmountValidator.validate(0).valid).to.deep.equal(false);
    });

    it("one multiple should return true", function(){
        expect(AmountValidator.validate(0, 26).valid).to.deep.equal(true);
    });

    it("non-multiple should return false with multiple option", function(){
        expect(AmountValidator.validate(2, 26).valid).to.deep.equal(false);
    });

    it("number between min and max should return true", function(){
        expect(AmountValidator.validate(60).valid).to.deep.equal(true);
    });

    it("number greater than max should return false", function(){
        expect(AmountValidator.validate(1000).valid).to.deep.equal(false);
    });

    it("number greater than max but also a multiple should return false", function(){
        expect(AmountValidator.validate(2600).valid).to.deep.equal(false);
    });
});