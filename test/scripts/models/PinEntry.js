var PinEntry = require('../../../src/scripts/models/PinEntry');
var expect = require('chai').expect;

describe("PinEntry", function(){

    it("should return all stars for no value", function(){
        expect(PinEntry.fromValue("", 4)).to.deep.equal("****");
    });

    it("should return one star for three digits", function(){
        expect(PinEntry.fromValue("123", 4)).to.deep.equal("*123");
    });

    it("should limit digits to 4", function(){
        expect(PinEntry.fromValue("12345", 4)).to.deep.equal("1234");
    });

});