var AmountEntry = require('../../../src/scripts/models/AmountEntry');
var expect = require('chai').expect;

describe("AmountEntry", function(){

    it("should return 0.00 for empty amount", function(){
        expect(AmountEntry.fromValue("")).to.deep.equal("0.00 €");
    });

    it("should comma format numbers", function(){
        expect(AmountEntry.fromValue(12345)).to.deep.equal("12,345.00 €");
    });

});