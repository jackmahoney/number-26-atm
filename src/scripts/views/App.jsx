var React = require('react');
var Enum = require('es6-enum');

var WelcomeScreen = require('./WelcomeScreen.jsx');
var ErrorScreen = require('./ErrorScreen.jsx');
var CardScreen = require('./CardScreen.jsx');
var PinEntryScreen = require('./PinEntryScreen.jsx');
var WithdrawScreen = require('./WithdrawScreen.jsx');
var PayoutScreen = require('./PayoutScreen.jsx');
var AbortScreen = require('./AbortScreen.jsx');


const STAGES = Enum("WELCOME", "CARD_ENTRY", "PIN_ENTRY", "WITHDRAW_AMOUNT", "PAYOUT", "ABORT");

module.exports = React.createClass({

    getInitialState: function () {
        return {
            stage: STAGES.WELCOME
        };
    },

    render: function () {
        var view;

        switch(this.state.stage){
            case STAGES.WELCOME:
                view = <WelcomeScreen onSuccess={ () => this.setState({stage: STAGES.CARD_ENTRY})}/>;
                break;
            case STAGES.CARD_ENTRY:
                view = <CardScreen
                    onSuccess={ () => this.setState({stage: STAGES.PIN_ENTRY})}
                    onAbort={ (hasCard) => this.setState({stage: hasCard ? STAGES.ABORT : STAGES.WELCOME})}
                />;
                break;
            case STAGES.PIN_ENTRY:
                view = <PinEntryScreen
                    onSuccess={ () => this.setState({stage: STAGES.WITHDRAW_AMOUNT})}
                    onAbort={ () => this.setState({stage: STAGES.ABORT})}
                />;
                break;
            case STAGES.WITHDRAW_AMOUNT:
                view = <WithdrawScreen
                    onSuccess={ () => this.setState({stage: STAGES.PAYOUT})}
                    onAbort={ () => this.setState({stage: STAGES.ABORT})}
                />;
                break;
            case STAGES.PAYOUT:
                view = <PayoutScreen
                    onSuccess={ () => this.setState({stage: STAGES.WELCOME})}
                />;
                break;
            case STAGES.ABORT:
                view = <AbortScreen
                    onSuccess={ () => this.setState({stage: STAGES.WELCOME})}
                />;
                break;
            default:
                view = <ErrorScreen onResolved={ () => this.setState({stage: STAGES.WELCOME}) }/>
        }

        return (
            <div className="app">{view}</div>
        );
    }

});
