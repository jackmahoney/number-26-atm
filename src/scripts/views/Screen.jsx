var React = require('react');

/**
 * A screen component with processing visuals
 */
class Screen extends React.Component {
    render() {
        return (
            <div className="screen col-sm-6">
                {this.props.processing &&
                    <div className="screen_processing">
                        <div className="screen_processing_icon"><img src="images/ajax-loader.gif"/></div>
                        <div className="screen_processing_message">{this.props.processingMessage}</div>
                    </div>
                }
                {this.props.children}
            </div>
        );
    }
}

Screen.propTypes = {
    processing: React.PropTypes.bool,
    processingMessage: React.PropTypes.string
};

module.exports = Screen;
