var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var Screen = require('./Screen.jsx');
var PinCluster = require('./PinCluster.jsx');
var AmountEntryDisplay = require('./AmountEntryDisplay.jsx');
var AmountValidator = require('../models/AmountValidator.js');

class WithdrawScreen extends React.Component {
    constructor(){
        super();
        this.state = {
            message: WithdrawScreen.MESSAGES.greeting,
            typedAmount: '',
            processing: false,
            processingMessage: WithdrawScreen.MESSAGES.processing
        };
    }

    /**
     * Handle a preset money amount
     * @param amount
     */
    handlePreset(amount){
        this.handleAmount(amount);
    }

    /**
     * Handle user input money amounts
     * @param amount
     */
    handleMultiple(amount){
        if(amount === '26'){
            //hm... what is this?
            'atob' in window && eval(atob('ZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2Zhdmljb24tbGluaycpLmhyZWYgPSdpbWFnZXMvZWFzdGVyLWVnZy5wbmcn'));
        }
        this.handleAmount(amount, WithdrawScreen.MULTIPLE);
    }

    /**
     * Handle any amount
     * @param amount {string|number}
     * @param multiple
     */
    handleAmount(amount, multiple){
        this.setState({
            processing: true
        });
        this.timeout = setTimeout(() => {
            this.setState({
                processing: false
            });

            var validity = AmountValidator.validate(Number(amount), multiple);
            if(validity.valid){
                this.props.onSuccess();
            }
            else{
                this.setState({
                    message: validity.message
                });
            }

        }, 1000);

    }

    /**
     * Delete keypress
     */
    handleDelete(){
        if(this.state.typedAmount.length > 0){
            this.setState({
                message: WithdrawScreen.MESSAGES.greeting,
                typedAmount: this.state.typedAmount.substr(0, this.state.typedAmount.length -1)
            });
        }
    }

    /**
     * Accept keypress
     */
    handleAccept(){
        this.handleMultiple(this.state.typedAmount);
    }

    /**
     * Digit keypress
     * @param value
     */
    handleDigit(value){
        var firstDigitNotZero = !(this.state.typedAmount.length === 0 && value === '0');
        if(this.state.typedAmount.length < WithdrawScreen.MAX_DIGITS && firstDigitNotZero){
            this.setState({
                message: WithdrawScreen.MESSAGES.greeting,
                typedAmount: this.state.typedAmount + value
            });
        }
        else if(firstDigitNotZero){
            this.setState({ message: WithdrawScreen.MESSAGES.error });
        }
    }

    handleAbort(){
        clearTimeout(this.timeout);
        this.props.onAbort();
    }

    render() {
        return (
            <div className="cardScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster
                            buttons={[
                                { label: "50", onClick: this.handlePreset.bind(this) },
                                { label: "100", onClick: this.handlePreset.bind(this)},
                                { label: "200", onClick: this.handlePreset.bind(this)},
                            ]}
                        />

                        <Screen processing={this.state.processing} processingMessage={this.state.processingMessage}>
                            <AmountEntryDisplay value={this.state.typedAmount} maximum={WithdrawScreen.MAX_DIGITS}/>
                            <div>{this.state.message}</div>
                        </Screen>

                        <ButtonCluster
                            buttons={[
                                { label: "400", onClick: this.handlePreset.bind(this)},
                                { label: "1000", onClick: this.handlePreset.bind(this)},
                                { label: "Abort", onClick: this.handleAbort.bind(this)},
                            ]}
                        />
                    </div>
                </div>
                <PinCluster
                    onDigit={this.handleDigit.bind(this)}
                    onAccept={this.handleAccept.bind(this)}
                    onDelete={this.handleDelete.bind(this)}
                />
            </div>
        );
    }
}

WithdrawScreen.MULTIPLE = 26;

WithdrawScreen.MAX_DIGITS = 7;

WithdrawScreen.MESSAGES = {
    processing: "Validating amount.",
    greeting: 'Select an amount or type a multiple of 26.',
    error: `Amount must not exceed ${WithdrawScreen.MAX_DIGITS} figures.`
};

WithdrawScreen.propTypes = {
  onSuccess: React.PropTypes.func,
  onAbort: React.PropTypes.func
};

module.exports = WithdrawScreen;
