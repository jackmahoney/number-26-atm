var React = require('react');
var PinButton = require('./PinButton.jsx');
var keycode = require('keycode');
var arrayChunk = require('chunk');
var classNames = require('classnames');

/**
 * PIN-pad cluster
 */
class PinCluster extends React.Component {

    constructor(){
        super();
        this.state = {
            //create an array of objects based on the labels
            buttons: PinCluster.buttonsTemplate.map((label) => {
                return {
                    label: '' + label,
                    active: false,
                    onClick: this.handleClick.bind(this)
                };
            })
        };
    }

    handleClick(value){
        if(value === PinCluster.buttonLabels.ACCEPT){
            this.props.onAccept && this.props.onAccept(value);
        }
        else if(value === PinCluster.buttonLabels.DELETE){
            this.props.onDelete && this.props.onDelete(value);
        }
        else{
            this.props.onDigit && this.props.onDigit(value);
        }
    }

    isDigit(string){
        return /\d/.test(string);
    }

    componentDidMount(){
        //reference to this for inside callback (can't bind in this instance)
        var _this = this;

        //assign callbacks to vars so we can remove them later
        this.keydownListener = (e) => {
            var code = keycode(e.keyCode);
            var soughtLabel = '';
            //if digit?
            if(this.isDigit(code)){
                soughtLabel = code;
            }
            else if(code === "delete" || code === "backspace"){
                soughtLabel = PinCluster.buttonLabels.DELETE;
                e.preventDefault();
            }
            else if(code === "enter" || code === "return"){
                soughtLabel = PinCluster.buttonLabels.ACCEPT;
            }
            if(soughtLabel){
                _this.setState({
                    buttons: _this.state.buttons.map((button) => {
                        button.active = button.label === soughtLabel;
                        return button;
                    })
                });

                _this.handleClick(soughtLabel);
            }
        };

        this.keyupListener = (e) => {
            _this.setState({
                buttons: _this.state.buttons.map((button) => {
                    button.active = false;
                    return button;
                })
            });
        }

        //handle keydowns to trigger active state on pinbuttons
        window.addEventListener('keydown', this.keydownListener);

        //remove any active button states
        window.addEventListener('keyup', this.keyupListener);
    }

    componentWillUnmount(){
        window.removeEventListener('keydown', this.keydownListener);
        window.removeEventListener('keyup', this.keyupListener);
    }

    render() {

        return (
            <div className="pinCluster">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-3"></div>
                        <div className="col-sm-6">
                        {arrayChunk(this.state.buttons, PinCluster.buttonChunkSize).map((chunk, i) => {
                            //wrap each chunk in a row
                            return (<div className="row" key={`pin-cluster-row-${i}`}>
                            { chunk.map((button, j) => {

                                //create grid classname
                                var className = classNames({
                                   ['col-sm-' + (12 / PinCluster.buttonChunkSize)]: true,
                                   ['pinButton-' + button.label.toLowerCase()]: !this.isDigit(button.label)
                                });

                                //render the pin button
                                return (
                                    <PinButton
                                        key={`pin-button-${j}`}
                                        className={className}
                                        label={button.label}
                                        active={button.active}
                                        onClick={button.onClick}
                                    />
                                );
                            }) }
                            </div>);
                        })}
                        </div>
                        <div className="col-sm-3"></div>
                    </div>
                </div>
            </div>
        );
    }
}


PinCluster.buttonChunkSize = 3;

PinCluster.buttonLabels = {
  DELETE: 'Delete',
  ACCEPT: 'Accept'
};

//the order of buttons to be rendered
PinCluster.buttonsTemplate = [1, 2, 3, 4, 5, 6, 7, 8, 9, PinCluster.buttonLabels.DELETE, 0, PinCluster.buttonLabels.ACCEPT];

PinCluster.propTypes = {
  onAccept: React.PropTypes.func,
  onDelete: React.PropTypes.func,
  onDigit: React.PropTypes.func
};

module.exports = PinCluster;
