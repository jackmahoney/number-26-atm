var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var PinCluster = require('./PinCluster.jsx');
var Screen = require('./Screen.jsx');

/**
 * Pay out the money screen
 */
class AbortScreen extends React.Component {
    constructor(){
        super();

        this.state = {
            processing: false,
            processingMessage: 'Returning your card.',
            message: 'Have a nice day!'
        };
    }

    componentDidMount(){
        this.setState({ processing: true });
        //fake money fetching
        setTimeout(() => {
            //show message then go to home screen
            this.setState({ processing: false });
            setTimeout(() => {
                this.props.onSuccess && this.props.onSuccess();
            }, 2000);
        }, 1000);
    }

    render() {
        return (
            <div className="payoutScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster buttons={[{},{},{}]}/>
                        <Screen processing={this.state.processing} processingMessage={this.state.processingMessage}>{this.state.message}</Screen>
                        <ButtonCluster buttons={[{},{},{}]}/>
                    </div>
                </div>
                <PinCluster/>
            </div>
        );
    }
}

AbortScreen.props = {
    onSuccess: React.PropTypes.func
};

module.exports = AbortScreen;
