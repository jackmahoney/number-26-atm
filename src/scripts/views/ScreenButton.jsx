var React = require('react');
var Button = require('./Button.jsx');

/**
 * A button that sits beside a screen
 */
class ScreenButton extends Button {
    render() {
        return (
            <div className="screenButton">
                <div className="screenButton_button" onClick={() => {
                    this.beep();
                    this.props.onClick && this.props.onClick(this.props.label);
                }}>
                    <div className="screenButton_label">{this.props.label}</div>
                </div>
            </div>
        );
    }
}

module.exports = ScreenButton;
