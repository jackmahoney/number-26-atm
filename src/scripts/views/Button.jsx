var React = require('react');

/**
 * A beeping button abstract
 */
class Button extends React.Component {
    constructor() {
        super();
        this.audio = new Audio("audio/beep-8.mp3");
    }
    beep(){
        this.audio.pause();
        this.audio.currentTime = 0;
        this.audio.play();
    }
}



module.exports = Button;
