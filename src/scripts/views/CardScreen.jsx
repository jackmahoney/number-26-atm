var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var Screen = require('./Screen.jsx');
var PinCluster = require('./PinCluster.jsx');

/**
 * Card insertion screen
 */
class CardScreen extends React.Component {
    constructor(){
        super();
        this.state = {
            hasCard: false,
            processing: false,
            message: 'Please insert your card. '
        };
    }
    handleAbort(){
        clearTimeout(this.cardTimeout);
        this.setState({
            processing: false
        });
        this.props.onAbort(this.state.hasCard);
    }
    handleCardInsertion(){
        this.setState({
            hasCard: true,
            processing: true,
            message: 'Validating your card. Please wait.'
        });
        //simulate card reading
        this.cardTimeout = setTimeout(() => {
            if(this.state.processing){
                this.setState({
                    processing: false
                });
                this.props.onSuccess();
            }
        }, 1000);
    }
    render() {
        return (
            <div className="cardScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster
                            buttons={[
                                {},
                                {},
                                { label: "Abort", onClick: this.handleAbort.bind(this)},
                            ]}
                        />
                        <Screen processing={this.state.processing} processingMessage={this.state.message}>
                            <div>
                                {this.state.message}
                                <a onClick={this.handleCardInsertion.bind(this)}>Simulate card insertion</a>
                            </div>
                        </Screen>
                        <ButtonCluster buttons={[{},{},{}]}/>
                    </div>
                </div>
                <PinCluster/>
            </div>
        );
    }
}

CardScreen.propTypes = {
  onSuccess: React.PropTypes.func,
  onAbort: React.PropTypes.func
};

module.exports = CardScreen;
