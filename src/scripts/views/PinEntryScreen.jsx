var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var Screen = require('./Screen.jsx');
var PinCluster = require('./PinCluster.jsx');
var PinEntryDisplay = require('./PinEntryDisplay.jsx');

/**
 * Stage in ATM process of entering and validating PIN
 */
class PinEntryScreen extends React.Component {

    constructor(){
        super();
        this.state = {
            processing: false,
            processingMessage: "",
            message: PinEntryScreen.MESSAGES.greeting,
            pinValue: ""
        };
    }

    /**
     * Validate this.state.pinValue
     */
    validatePin(){
        this.setState({
            processing: true,
            processingMessage: PinEntryScreen.MESSAGES.validating
        });
        //simulate processing wait time
        this.timeout = setTimeout(() => {
            this.setState({
                processing: false,
                processingMessage: ''
            });

            //we would use some call to the backend or hardware here, faked for demo purposes
            if(this.state.pinValue === "1234"){
                this.props.onSuccess();
            }
            else{
                this.setState({
                    pinValue: "",
                    message: PinEntryScreen.MESSAGES.incorrect
                });
            }
        }, 1000);
    }

    handleDelete(){
        if(this.state.pinValue.length > 0){
            this.setState({
                pinValue: this.state.pinValue.substr(0, this.state.pinValue.length -1),
                message: PinEntryScreen.MESSAGES.greeting
            });
        }
    }

    handleAccept(){
        if(this.state.pinValue.length === PinEntryDisplay.MAX_DIGITS){
            this.validatePin();
        }
        else{
            this.setState({ message: PinEntryScreen.MESSAGES.lengthError})
        }
    }

    handleDigit(value){
        if(this.state.pinValue.length < PinEntryDisplay.MAX_DIGITS){
            var newValue = this.state.pinValue + value;
            this.setState({
                pinValue: newValue,
                message: newValue.length === PinEntryDisplay.MAX_DIGITS ? PinEntryScreen.MESSAGES.prompt : PinEntryScreen.MESSAGES.greeting
            });
        }
    }

    handleAbort(){
        clearTimeout(this.timeout);
        this.props.onAbort();
    }

    render() {
        return (
            <div className="cardScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster
                            buttons={[
                                {},
                                {},
                                { label: "Abort", onClick: () => this.handleAbort() },
                            ]}
                        />
                        <Screen processing={this.state.processing} processingMessage={this.state.processingMessage}>
                            <PinEntryDisplay value={this.state.pinValue}/>
                            <div>{this.state.message}</div>
                        </Screen>
                        <ButtonCluster buttons={[{},{},{}]}/>
                    </div>
                </div>
                <PinCluster
                    onDigit={this.handleDigit.bind(this)}
                    onAccept={this.handleAccept.bind(this)}
                    onDelete={this.handleDelete.bind(this)}
                />
            </div>
        );
    }
}

PinEntryScreen.MESSAGES = {
    validating: "Validating your PIN.",
    greeting: "Please enter your PIN (using the PIN-pad or keyboard). Try 1234.",
    prompt: "Please press Accept.",
    lengthError: `PIN must be ${PinEntryDisplay.MAX_DIGITS} in length.`,
    incorrect: "Incorrect PIN. Please try again."
};

PinEntryScreen.propTypes = {
  onSuccess: React.PropTypes.func,
  onAbort: React.PropTypes.func
};

module.exports = PinEntryScreen;
