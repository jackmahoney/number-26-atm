var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var PinCluster = require('./PinCluster.jsx');
var Screen = require('./Screen.jsx');

class ErrorScreen extends React.Component {
    render() {
        return (
            <div className="errorScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster
                            buttons={[
                                {},
                                {},
                                { label: "Home", onClick: () => { this.props.onResolved() } }
                            ]}
                        />
                        <Screen><span>ERROR!</span></Screen>
                        <ButtonCluster buttons={[{},{},{}]}/>
                    </div>
                </div>
                <PinCluster/>
            </div>
        );
    }
}

module.exports = ErrorScreen;
