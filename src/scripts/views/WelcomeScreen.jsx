var React = require('react');
var ButtonCluster = require('./ButtonCluster.jsx');
var Screen = require('./Screen.jsx');
var PinCluster = require('./PinCluster.jsx');

/**
 * Welcome screen
 */
class WelcomeScreen extends React.Component {
    render() {
        return (
            <div className="welcomeScreen">
                <div className="container">
                    <div className="row">
                        <ButtonCluster
                            buttons={[
                                { label: "Withdraw", onClick: () => this.props.onSuccess() },
                                {}, //no other options for this demo, only withdraw
                                {}
                            ]}
                        />
                        <Screen>
                            <span>Welcome!</span>
                        </Screen>
                        <ButtonCluster buttons={[{},{},{}]}/>
                    </div>
                </div>
                <PinCluster/>
            </div>
        );
    }
}

module.exports = WelcomeScreen;
