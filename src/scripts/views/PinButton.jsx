var React = require('react');
var Button = require('./Button.jsx');
var classNames = require('classnames');

/**
 * A PIN button
 */
class PinButton extends Button {
    handleClick() {
        this.beep();
        this.props.onClick(this.props.label);
    }
    render() {
        var classes = classNames({
            'pinButton': true,
            [this.props.className]: this.props.className,
            'pinButton-active': this.props.active
        });
        return (
            <div className={classes}>
                <div className="pinButton_inner" onClick={this.handleClick.bind(this)}>{this.props.label}</div>
            </div>
        );
    }

}

PinButton.propTypes = {
    onClick: React.PropTypes.func,
    label: React.PropTypes.string,
    active: React.PropTypes.bool
};

module.exports = PinButton;
