var React = require('react');
var classNames = require('classnames');
var PinEntry = require('../models/PinEntry');

/**
 * Show entry of PIN
 */
class PinEntryDisplay extends React.Component {
    getValue() {
        return PinEntry.fromValue(this.props.value, PinEntryDisplay.MAX_DIGITS);
    }
    render() {
        var classes = classNames({
            'pinEntryDisplay': true,
            [this.props.className]: this.props.className
        });
        return (
            <div className={classes}>{this.getValue()}</div>
        );
    }

}

PinEntryDisplay.MAX_DIGITS = 4;

PinEntryDisplay.defaultProps = {
    value: ""
};
PinEntryDisplay.propTypes = {
    value: React.PropTypes.oneOfType([ React.PropTypes.string, React.PropTypes.number])
};

module.exports = PinEntryDisplay;
