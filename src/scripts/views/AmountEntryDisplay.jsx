var React = require('react');
var classNames = require('classnames');
var AmountEntry = require('../models/AmountEntry');

/**
 * Display a currency amount
 */
class AmountEntryDisplay extends React.Component {
    getValue() {
        return AmountEntry.fromValue(this.props.value);
    }
    render() {
        var classes = classNames({
            'amountEntryDisplay': true,
            [this.props.className]: this.props.className
        });
        return (
            <div className={classes}>{this.getValue()}</div>
        );
    }

}

AmountEntryDisplay.MAX_DIGITS = 4;

AmountEntryDisplay.defaultProps = {
    value: 0
};
AmountEntryDisplay.propTypes = {
    value: React.PropTypes.oneOfType([React.PropTypes.number, React.PropTypes.string])
};

module.exports = AmountEntryDisplay;
