var React = require('react');
var ScreenButton = require('./ScreenButton.jsx');

/**
 * Cluster of buttons
 */
class ButtonCluster extends React.Component {
    render() {
        return (
            <div className="buttonCluster col-sm-3">
                {this.props.buttons.map((v, i) => <ScreenButton key={`screen-button-${i}`} label={v.label} onClick={v.onClick}/>)}
            </div>
        );
    }
}

ButtonCluster.defaultProps = { buttons: [] };

module.exports = ButtonCluster;
