var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./views/App.jsx');

//render the App into a container
ReactDOM.render(
  <App/>, document.getElementById('container')
);

console.log('%c Hello, hacker! Take a look around...', 'color: rebeccapurple; font-size:12px;');