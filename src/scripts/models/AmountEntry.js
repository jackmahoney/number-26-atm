var AmountEntry = {

    /**
     * Create a currency display for a number
     * @param value
     * @returns {string}
     */
    fromValue: function(value){
        value = value || 0;
        return Number(value).toLocaleString() + ".00 €";
    }

};

module.exports = AmountEntry;