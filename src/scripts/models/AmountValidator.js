/**
 * @param valid
 * @param message
 * @constructor
 */
var Validity = function(valid, message){
    this.valid = valid;
    this.message = message;
};

/**
 * Amount Validator
 */
var AmountValidator = {

    MAX: 800,
    MIN: 50,

    /**
     * Validate an amount or multiple
     * @param amount
     * @param multiple
     * @returns {Validity}
     */
    validate: function(amount, multiple){
        if(multiple && amount % multiple === 0 && amount <= this.MAX){
            return new Validity(true);
        }
        else if(multiple && amount % multiple !== 0){
            return new Validity(false, `Amount must be a multiple of ${multiple}.`);
        }
        else if(amount > this.MAX){
            return new Validity(false, `The requested amount exceeds your account maximum (${this.MAX}). Maybe it's time for a new job?`);
        }
        else if((!multiple && amount < this.MIN) || (multiple && amount < multiple)){
            return new Validity(false, `Amount must be at least ${this.MIN} or a multiple of ${multiple}`);
        }
        else{
            return new Validity(true);
        }
    }

};

module.exports = AmountValidator;