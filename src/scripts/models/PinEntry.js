var PinEntry = {

    REGEX: /[^\d]/g,

    /**
     * Get a pin entry string from a string of numbers
     * @param value
     * @returns {string}
     */
    fromValue: function(value, maxDigits){
        value = value || '';

        //prepare an array of stars
        var results = [];

        //take the first four digits from the input
        var digits = value.replace(this.REGEX,'').split('').slice(0, maxDigits);

        //feed the input into the stars array right to left
        digits.reverse();
        for (var i = 0; i < maxDigits; i++) {
            results.push(digits[i] ? digits[i] : '*');
        }
        results.reverse();

        return results.join('');
    }

};

module.exports = PinEntry;