#Number 26 ATM
[Jack Mahoney](http://dev.jackmahoney.me)'s submission for the ATM prototype developer challenge.

It simulates the behaviour of an ATM, allowing a user to enter a card, give a PIN, and withdraw money.
Due to demo purposes, functionality is limited.

Architecturally, it's a React SPA built using ES6, JSX, SCSS, compiled using Gulp, Babel, Browserify.
Follow the compilation steps below in order to run it, or you can find a live version [running here](http://dev
.jackmahoney.me/__projects/number-26-atm/).

##Setup
###Installation
Install NodeJS.

`npm install`

###Running
`npm run serve`

###Testing
`npm run test`

##Caveats
Many validation functions are faked. We don't have a real backend and it's only a demo. Likewise, no concern was paid
to security; in a real application this would be different.

If I had more time, I would expand test coverage. Currently, I only unit test my non-rendering component logic. For a
real project I would also test the React components. For simplicity I have also used explicit one-way bindings. In a
more complex app, I might have chosen to use Redux stores. One advantage of the stores would be easy global updates,
for instance language changes mid-stage.

Card insertion simulation is triggered by a button and kept minimal for time reasons. I presented 5 preset money
amounts instead of 6 for design reasons. Styles were kept very simple as I aimed to demonstrate logic more than styles.
This project is also **not responsive** and should be viewed on a device at least 1200px wide. It was only tested on
**Chrome on Mac**. A real application would be thoroughly cross browser tested.

Sound is played only for fun. I haven't optimised for production (minification etc) for readability.