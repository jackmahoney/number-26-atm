var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var babelify = require('babelify');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var copy = require('gulp-copy');
var open = require('gulp-open');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');

gulp.task('javascript', function () {
  var b = browserify({
    entries: './src/scripts/main.jsx',
    debug: true
  });

  return b
    .transform(babelify, { presets: ['babel-preset-react', 'babel-preset-es2015']})
    .bundle()
    .pipe(source('main.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    //.pipe(uglify())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('copy', function () {
    gulp.src('./src/html/index.html')
        .pipe(copy('./dist', { prefix: 2 }));
    gulp.src('./src/images/**')
        .pipe(copy('./dist', { prefix: 1}));
    gulp.src('./src/audio/**')
        .pipe(copy('./dist', { prefix: 1}));
});

gulp.task('sass', function () {
    gulp.src('./src/styles/main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('serve', ['copy', 'sass','javascript'], function(){
    gulp.src('./dist/index.html').pipe(open());
});

gulp.task('watch', ['serve'], function () {
    gulp.watch('./src/html/**/*', ['copy']);
    gulp.watch('./src/styles/**/*.scss', ['sass']);
    gulp.watch('./src/scripts/**/*', ['javascript']);
});